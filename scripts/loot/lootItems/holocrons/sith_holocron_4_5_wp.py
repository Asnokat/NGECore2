def itemTemplate():

	return ['object/tangible/loot/creature_loot/collections/shared_sith_holocron_01.iff']

def lootDescriptor():

    return 'customattributes'
        
def customizationAttributes():
    return ['/private/index_color_1']
    
def customizationValues():
    return [10]  
    
def STFparams():

	return ['static_item_n','item_collection_sith_holocron_02_04','static_item_d','item_collection_sith_holocron_02_04']